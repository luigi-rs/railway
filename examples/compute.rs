use railway::Program;
use railway::couple::Couple;
use std::env;
use std::fs::File;
use std::io::Read;

fn parse_args() -> (String, Option<String>) {
	let mut args = env::args();
	let mut railway_file = None;
	let mut arguments_file = None;
	while let Some(s) = args.next() {
		match s.as_str() {
			"-f" => railway_file = args.next(),
			"-a" => arguments_file = args.next(),
			_ => ()
		}
	}
	(railway_file.unwrap(), arguments_file)
}

fn main() {
	let (rwy_path, _args_path) = parse_args();
	let mut bytes = Vec::new();
	File::open(rwy_path).unwrap().read_to_end(&mut bytes).unwrap();
	let program = Program::parse(&bytes).unwrap();
	let arguments = vec![("size", Couple::from((100f32, 50f32)))];
	let result = program.run(&arguments).unwrap();
	println!("{:#?}", result);
}
