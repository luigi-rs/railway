use crate::couple::Couple;

#[derive(Debug, Clone, Copy)]
pub struct TrianglePoint {
	pub coords: Couple,
	pub color: (f32, f32, f32, f32),
}

#[derive(Debug, Clone, Copy)]
pub struct Triangle {
	pub p1: TrianglePoint,
	pub p2: TrianglePoint,
	pub p3: TrianglePoint,
}

#[derive(Debug, Clone, Copy)]
pub struct CubicBezierCurve {
	pub p1: Couple,
	pub p2: Couple,
	pub p3: Couple,
	pub p4: Couple,
}

#[derive(Debug, Clone, Copy)]
pub struct Mask {
	pub side1: CubicBezierCurve,
	pub side2: CubicBezierCurve,
}
