// https://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle
pub mod label;
pub mod operation;
pub mod clip;
pub mod couple;
pub mod computed;

use std::convert::TryFrom;
use operation::Operation;
use operation::Instruction;
use label::Label;
use clip::Triangle;
use clip::Mask;
use clip::Clip;
use couple::Couple;

type Addr = u32;

#[derive(Debug)]
pub struct Program {
	initial_stack: Vec<Couple>,
	arguments: Vec<Label>,
	outputs: Vec<Label>,
	operations: Vec<Operation>,
	clips: Vec<Clip>,
}

#[derive(Debug)]
pub enum ParsingError {
	NotARailwayFile,
	TooShort,
	InvalidAddress,
	InvalidInstruction,
	InvalidTriangle,
	InvalidMask,
	InvalidLabelName,
	NoArguments,
}

use ParsingError::*;

const RAILWAY_MAGIC: [u8; 4] = [b'R', b'W', b'Y', b'0'];

fn try_subslice(data: &[u8], start: usize, stop: usize) -> Result<&[u8], ParsingError> {
	// data[start] is assumed to exist
	match data.len() >= stop {
		true => Ok(data.split_at(stop).0.split_at(start).1),
		false => Err(TooShort),
	}
}

fn try_u32<'a>(data: &'a [u8], index: &mut usize) -> Result<u32, ParsingError> {
	let idxbck = *index;
	*index += 4;
	let u8x4 = try_subslice(data, idxbck, *index)?;
	let bytes: [u8; 4] = <[u8; 4]>::try_from(u8x4).unwrap();
	Ok(u32::from_be_bytes(bytes))
}

fn try_f32<'a>(data: &'a [u8], index: &mut usize) -> Result<f32, ParsingError> {
	let idxbck = *index;
	*index += 4;
	let u8x4 = try_subslice(data, idxbck, *index)?;
	let bytes: [u8; 4] = <[u8; 4]>::try_from(u8x4).unwrap();
	Ok(f32::from_be_bytes(bytes))
}

fn try_n_u32<'a>(data: &'a [u8], index: &mut usize, n: usize) -> Result<Vec<u32>, ParsingError> {
	let mut values = Vec::with_capacity(n);
	for _ in 0..n {
		values.push(try_u32(data, index)?);
	}
	Ok(values)
}

fn find_zero(data: &[u8], start: usize) -> Result<usize, ParsingError> {
	for i in start..data.len() {
		if data[i] == 0 {
			return Ok(i)
		}
	}
	Err(TooShort)
}

impl Program {
	pub fn parse(bytes: &Vec<u8>) -> Result<Self, ParsingError> {
		let data = match bytes.strip_prefix(&RAILWAY_MAGIC) {
			Some(data) => Ok(data),
			None => Err(NotARailwayFile),
		}?;
		let mut i = 0;
		let i = &mut i;
		// ARGUMENTS
		let arguments_count = try_u32(data, i)?;
		if arguments_count == 0 {
			Err(NoArguments)?;
		}
		let mut initial_stack = Vec::with_capacity(arguments_count as usize);
		for _ in 0..arguments_count {
			let x = try_f32(data, i)?;
			let y = try_f32(data, i)?;
			initial_stack.push(Couple::from((x, y)));
		}
		let mut max_stack_address = arguments_count;
		// OPERATIONS
		let operations = {
			let operations_count = try_u32(data, i)?;
			let mut operations = Vec::with_capacity(operations_count as usize);
			for _ in 0..operations_count {
				let instruction = try_u32(data, i)?;
				let operands = try_n_u32(data, i, 3)?;
				let instruction = Instruction::try_from(instruction);
				let instruction = instruction.ok_or(InvalidInstruction)?;
				instruction.filter(&operands).ok_or(InvalidAddress)?;
				if *operands.iter().max().unwrap() > max_stack_address {
					Err(InvalidAddress)?;
				}
				operations.push(Operation::new(instruction, &operands));
				max_stack_address += 1;
			}
			operations
		};
		// OUTPUTS
		let outputs_count = try_u32(data, i)?;
		let mut outputs = Vec::with_capacity(outputs_count as usize);
		for _ in 0..outputs_count {
			let address = try_u32(data, i)?;
			if address > max_stack_address {
				Err(InvalidAddress)?;
			}
			outputs.push(Label::new(address));
		}
		// CLIPS
		let clips = {
			// TRIANGLES
			let triangles = {
				let triangles_count = try_u32(data, i)?;
				let mut triangles = Vec::with_capacity(triangles_count as usize);
				for _ in 0..triangles_count {
					let p1 = try_n_u32(data, i, 3)?;
					let p2 = try_n_u32(data, i, 3)?;
					let p3 = try_n_u32(data, i, 3)?;
					let p1 = <[u32; 3]>::try_from(p1).unwrap();
					let p2 = <[u32; 3]>::try_from(p2).unwrap();
					let p3 = <[u32; 3]>::try_from(p3).unwrap();
					if *p1.iter().max().unwrap() > max_stack_address
					|| *p2.iter().max().unwrap() > max_stack_address
					|| *p3.iter().max().unwrap() > max_stack_address {
						Err(InvalidAddress)?;
					}
					triangles.push(Triangle::new(p1, p2, p3));
				}
				triangles
			};
			// MASKS
			let masks = {
				let masks_count = try_u32(data, i)?;
				let mut masks = Vec::with_capacity(masks_count as usize);
				for _ in 0..masks_count {
					let side1 = try_n_u32(data, i, 4)?;
					let side2 = try_n_u32(data, i, 4)?;
					let side1 = <[u32; 4]>::try_from(side1).unwrap();
					let side2 = <[u32; 4]>::try_from(side2).unwrap();
					if *side1.iter().max().unwrap() > max_stack_address
					|| *side2.iter().max().unwrap() > max_stack_address {
						Err(InvalidAddress)?;
					}
					masks.push(Mask::new(side1, side2));
				}
				masks
			};
			let clips_count = try_u32(data, i)?;
			let mut clips = Vec::with_capacity(clips_count as usize);
			for _ in 0..clips_count {
				let triangle = try_u32(data, i)? as usize;
				let mask     = try_u32(data, i)? as usize;
				if triangle > triangles.len() {
					Err(InvalidTriangle)?;
				}
				if mask > masks.len() {
					Err(InvalidMask)?;
				}
				clips.push(Clip::new(triangles[triangle], masks[mask]));
			}
			clips
		};
		// ARGUMENTS AGAIN YES
		let arguments = {
			let mut arguments = Vec::with_capacity(arguments_count as usize);
			for j in 0..arguments_count {
				let start = *i;
				let stop = find_zero(data, start)?;
				let len = stop - start;
				let mut lbl = Label::new(j);
				if len != 0 {
					let arg_name = try_subslice(data, start, stop)?;
					let arg_name = arg_name.to_vec();
					let arg_name = String::from_utf8(arg_name.to_vec()).ok();
					let arg_name = arg_name.ok_or(InvalidLabelName)?;
					lbl.set_name(arg_name);
				}
				arguments.push(lbl);
				*i = stop + 1;
			}
			arguments
		};
		// OUTPUTS NAMES
		for j in 0..outputs_count {
			let start = *i;
			let stop = find_zero(data, start)?;
			let len = stop - start;
			if len != 0 {
				let output_name = try_subslice(data, start, stop)?;
				let output_name = output_name.to_vec();
				let output_name = String::from_utf8(output_name.to_vec()).ok();
				let output_name = output_name.ok_or(InvalidLabelName)?;
				outputs[j as usize].set_name(output_name);
			}
			*i = stop + 1;
		}
		Ok(Program {
			initial_stack,
			arguments,
			outputs,
			operations,
			clips,
		})
	}
	
	pub fn run(&self, arguments: &[(&str, Couple)]) -> Option<(
		Vec<(String, Couple)>,
		Vec::<(computed::Triangle, computed::Mask)>
	)> {
		let mut stack = self.initial_stack.clone();
		for (name, value) in arguments {
			let i = Label::find(&self.arguments, &name)?;
			stack[self.arguments[i].address() as usize] = *value;
		}
		for o in &self.operations {
			stack.push(o.compute(&stack));
		}
		let mut clips = Vec::with_capacity(self.clips.len());
		for c in &self.clips {
			clips.push(c.compute(&stack));
		}
		let mut outputs = Vec::with_capacity(self.outputs.len());
		for o in &self.outputs {
			if let Some(name) = o.name() {
				outputs.push((name, stack[o.address() as usize]));
			}
		}
		Some((outputs, clips))
	}
}
