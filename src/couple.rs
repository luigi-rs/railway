#[derive(Debug, Clone, Copy)]
pub struct Couple {
	pub x: f32,
	pub y: f32,
}

impl From<(f32, f32)> for Couple {
	fn from(couple: (f32, f32)) -> Self {
		Couple {
			x: couple.0,
			y: couple.1,
		}
	}
}

pub const C_ZERO: Couple = Couple {
	x: 0f32,
	y: 0f32
};
