use crate::Addr;
use crate::couple::Couple;
use crate::computed;

#[derive(Debug, Clone, Copy)]
pub struct TrianglePoint {
	coords: Addr,
	rgba: (Addr, Addr),
}

#[derive(Debug, Clone, Copy)]
pub struct Triangle {
	p1: TrianglePoint,
	p2: TrianglePoint,
	p3: TrianglePoint,
}

#[derive(Debug, Clone, Copy)]
pub struct Mask {
	// two quadratic bezier curves
	side1: (Addr, Addr, Addr, Addr),
	side2: (Addr, Addr, Addr, Addr),
}

#[derive(Debug, Clone, Copy)]
pub struct Clip {
	triangle: Triangle,
	mask: Mask,
}

impl TrianglePoint {
	pub fn compute(&self, stack: &[Couple]) -> computed::TrianglePoint {
		let rg = stack[self.rgba.0 as usize];
		let ba = stack[self.rgba.1 as usize];
		computed::TrianglePoint {
			coords: stack[self.coords as usize],
			color: (rg.x, rg.y, ba.x, ba.y),
		}
	}
}

impl Triangle {
	pub fn new(p1: [u32; 3], p2: [u32; 3], p3: [u32; 3]) -> Self {
		Self {
			p1: TrianglePoint {
				coords: p1[0],
				rgba: (p1[1], p1[2]),
			},
			p2: TrianglePoint {
				coords: p2[0],
				rgba: (p2[1], p2[2]),
			},
			p3: TrianglePoint {
				coords: p3[0],
				rgba: (p3[1], p3[2]),
			},
		}
	}
	
	pub fn compute(&self, stack: &[Couple]) -> computed::Triangle {
		computed::Triangle {
			p1: self.p1.compute(stack),
			p2: self.p2.compute(stack),
			p3: self.p3.compute(stack),
		}
	}
}

impl Mask {
	pub fn new(side1: [u32; 4], side2: [u32; 4]) -> Self {
		Self {
			side1: (side1[0], side1[1], side1[2], side1[3]),
			side2: (side2[0], side2[1], side2[2], side2[3]),
		}
	}
	
	pub fn compute(&self, stack: &[Couple]) -> computed::Mask {
		computed::Mask {
			side1: computed::CubicBezierCurve {
				p1: stack[self.side1.0 as usize],
				p2: stack[self.side1.1 as usize],
				p3: stack[self.side1.2 as usize],
				p4: stack[self.side1.3 as usize],
			},
			side2: computed::CubicBezierCurve {
				p1: stack[self.side2.0 as usize],
				p2: stack[self.side2.1 as usize],
				p3: stack[self.side2.2 as usize],
				p4: stack[self.side2.3 as usize],
			},
		}
	}
}

impl Clip {
	pub fn new(triangle: Triangle, mask: Mask) -> Self {
		Self {
			triangle,
			mask,
		}
	}
	
	pub fn compute(&self, stack: &[Couple]) -> (computed::Triangle, computed::Mask) {
		(self.triangle.compute(stack), self.mask.compute(stack))
	}
}
