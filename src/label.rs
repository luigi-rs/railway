use crate::Addr;

#[derive(Debug)]
pub struct Label {
	address: Addr,
	name: Option<String>,
}

impl Label {
	pub fn new(address: Addr) -> Self {
		Self {
			address,
			name: None,
		}
	}

	pub fn set_name(&mut self, name: String) {
		self.name = Some(name);
	}
	
	pub fn address(&self) -> Addr {
		self.address
	}
	
	pub fn name(&self) -> Option<String> {
		match &self.name {
			Some(s) => Some(s.clone()),
			None => None,
		}
	}

	pub fn find(labels: &[Self], name: &str) -> Option<usize> {
		for i in 0..labels.len() {
			if labels[i].name.is_some() && labels[i].name.as_ref().unwrap() == name {
				return Some(i)
			}
		}
		None
	}
}
