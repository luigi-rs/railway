use std::cmp::Ordering;
use crate::couple::Couple;
use crate::Addr;

#[derive(Debug, Clone, Copy)]
pub enum Instruction {
	Add2,				// add two vectors
	Subtract2,			// subtract two vectors
	Multiply2,			// multiply two vectors
	Divide2,			// divide two vectors
	Select2,			// X of op1 and Y of op2
	Polar1,				// convert cartesian coordinates to polar ones
	Cartesian1,			// convert polar coordinates to cartesian ones
	Cartesian2,			// convert polar coordinates to cartesian ones and add to a vector
	Inside3,			// (1, 0) if op1 is inside of rectangle, else (0, 1)
	Swap1,				// swap X and Y
	Adjusted3,
}

impl Instruction {
	pub fn try_from(i: u32) -> Option<Self> {
		match i {
			0x0 => Some(Add2),
			0x1 => Some(Subtract2),
			0x2 => Some(Multiply2),
			0x3 => Some(Divide2),
			0x4 => Some(Select2),
			0x5 => Some(Polar1),
			0x6 => Some(Cartesian1),
			0x8 => Some(Cartesian2),
			0x9 => Some(Inside3),
			0xA => Some(Swap1),
			0xB => Some(Adjusted3),
			_ => None,
		}
	}

	pub fn filter(self, operands: &[Addr]) -> Option<()> {
		// on instructions with less than 3 operands, fake operands addresses
		// are null. there can be no instruction if there is nothing in stack,
		// so this is not a problem.
		let max = match self {
			Polar1 | Cartesian1 | Swap1 => 1,
			Add2 | Subtract2 | Multiply2 | Divide2 | Select2 | Cartesian2 => 2,
			Inside3 | Adjusted3 => 3,
		};
		for i in max..3 {
			if operands[i] != 0 {
				None?
			}
		}
		Some(())
	}
}

use Instruction::*;

#[derive(Debug, Clone, Copy)]
pub struct Operation {
	instruction: Instruction,
	op1: Addr,
	op2: Addr,
	op3: Addr,
}

impl Operation {
	pub fn new(instruction: Instruction, operands: &[Addr]) -> Self {
		Self {
			instruction,
			op1: operands[0],
			op2: operands[1],
			op3: operands[2],
		}
	}

	fn cartesian1(a: Couple) -> (f32, f32) {
		let tmp = a.x.sin_cos();
		(tmp.1 * a.y, tmp.0 * a.y)
	}

	fn add2(a: Couple, b: Couple) -> (f32, f32) {
		(a.x + b.x, a.y + b.y)
	}

	pub fn compute(&self, stack: &[Couple]) -> Couple {
		// on instructions with less than 3 operands, fake operands addresses
		// are null. there can be no instruction if there is nothing in stack,
		// so this is not a problem.
		let a = stack[self.op1 as usize];
		let b = stack[self.op2 as usize];
		let c = stack[self.op3 as usize];
		let tuple = match self.instruction {
			Add2 => Self::add2(a, b),
			Subtract2 => (a.x - b.x, a.y - b.y),
			Multiply2 => (a.x * b.x, a.y * b.y),
			Divide2 => (a.x / b.x, a.y / b.y),
			Select2 => (a.x, b.y),
			Polar1 => (a.y.atan2(a.x), (a.x * a.x + a.y * a.y).sqrt()),
			Cartesian1 => Self::cartesian1(a),
			Cartesian2 => Self::add2(Couple::from(Self::cartesian1(a)), b),
			Inside3 => {
				let bx = a.x.partial_cmp(&b.x);
				let by = a.y.partial_cmp(&b.y);
				let cx = a.x.partial_cmp(&c.x);
				let cy = a.y.partial_cmp(&c.y);
				match (bx, by, cx, cy) {
					(
					 Some(Ordering::Greater),
					 Some(Ordering::Greater),
					 Some(Ordering::Less),
					 Some(Ordering::Less),
					) => (1f32, 0f32),
					_ => (0f32, 1f32),
				}
			},
			Swap1 => (a.y, a.x),
			Adjusted3 => Self::add2(Couple::from((a.x * c.x, a.y * c.x)), Couple::from((b.x * c.y, b.y * c.y))),
		};
		Couple::from(tuple)
	}
}
