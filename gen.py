import struct

def u32(val):
	return struct.pack(">I", val)

def f32(val):
	return struct.pack(">f", val)

header = b'RWY0'

def create_file(arguments, operations, outputs, triangles, masks, clips):
	data = header

	data += u32(len(arguments))
	for i in arguments:
		data += f32(i[1][0])
		data += f32(i[1][1])

	data += u32(len(operations))
	for i in operations:
		data += u32(i[0])
		data += u32(i[1])
		data += u32(i[2])
		data += u32(i[3])

	data += u32(len(outputs))
	for i in outputs:
		data += u32(i[1])

	data += u32(len(triangles))
	for i in triangles:
		data += u32(i[0][0])
		data += u32(i[0][1])
		data += u32(i[0][2])
		data += u32(i[1][0])
		data += u32(i[1][1])
		data += u32(i[1][2])
		data += u32(i[2][0])
		data += u32(i[2][1])
		data += u32(i[2][2])

	data += u32(len(masks))
	for i in masks:
		data += u32(i[0][0])
		data += u32(i[0][1])
		data += u32(i[0][2])
		data += u32(i[0][3])
		data += u32(i[1][0])
		data += u32(i[1][1])
		data += u32(i[1][2])
		data += u32(i[1][3])

	data += u32(len(clips))
	for i in clips:
		data += u32(i[0])
		data += u32(i[1])

	for i in arguments:
		data += bytes(i[0], 'utf8') + b'\x00'

	for i in outputs:
		data += bytes(i[0], 'utf8') + b'\x00'
	
	return data

def main():
	arguments = [
		['', [0, 0]],
		['size', [100, 100]],
		['', [1, 0]],
		['', [0, 1]],
		['', [0.5, 0.5]],
	]

	MUL = 2
	NOADDR = 0
	ZERO = 0
	SIZE = 1
	X_FILT = 2
	Y_FILT = 3
	BLACK_RG = 0
	BLACK_BA = 3
	HALF = 4

	operations = [
		[MUL, X_FILT, SIZE, NOADDR],
		[MUL, Y_FILT, SIZE, NOADDR],
		[MUL, HALF, SIZE, NOADDR],
	]
	
	TOP_LEFT = ZERO
	TOP_RIGHT = 5
	BTM_LEFT = 6
	BTM_RIGHT = SIZE
	CENTER = 7

	outputs = []

	triangles = [
		[
			[TOP_LEFT,  BLACK_RG, BLACK_BA],
			[BTM_RIGHT,  BLACK_RG, BLACK_BA],
			[BTM_LEFT, BLACK_RG, BLACK_BA]
		],
	]

	masks = [
		[
			[TOP_RIGHT, CENTER, CENTER, BTM_RIGHT],
			[TOP_LEFT, CENTER, CENTER, BTM_LEFT],
		],
	]
	
	clips = [[0, 0]]
	
	content = create_file(arguments, operations, outputs, triangles, masks, clips)
	f = open('test.rwy', 'wb')
	f.write(content)
	f.close()

main()
